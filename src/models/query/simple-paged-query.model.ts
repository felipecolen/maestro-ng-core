import { Restriction } from './restriction.model';
import { Sort } from './sort.model';

export class SimplePagedQuery {
    public firstResult: number;
    public maxResult: number;
    public sorts: Sort[] = [];
    public restrictions: Array<Restriction<any>> = [];
}
