export enum RestrictionPattern {
  DATE = <any> 'DATE',
  TIMESTAMP = <any> 'TIMESTAMP',
  TIME = <any> 'TIME'
}
