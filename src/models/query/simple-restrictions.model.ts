import { IRestrictions } from './restrictions.interface';
import { Restriction } from './restriction.model';

export class SimpleRestrictions implements IRestrictions {

  public constantRestrictions: Array<Restriction<any>>;

  constructor(
    private restrictions: Array<Restriction<any>>,
    constantRestrictions?: Array<Restriction<any>>
  ) {
    if (constantRestrictions !== undefined) {
      this.constantRestrictions = constantRestrictions;
      this.constantRestrictions.forEach(r => {
        if (!r.isEnable()) {
          console.error('O valor da restrição ${r.attribute} não foi definido!');
        }
      });
    }
    else {
      this.constantRestrictions = [];
    }
  }

  public attr(attribute: string): Restriction<any> {
    return this.restrictions.filter(item => item.attribute === attribute)[0];
  }

  public getEnables(): Array<Restriction<any>> {

    const result: Array<Restriction<any>> = [];

    this.constantRestrictions.forEach(r => result.push(r));

    this.restrictions.forEach(restriction => {
      if (restriction.isEnable()) {
        result.push(restriction);
      }
    });

    return result;
  }

  public clear(): void {
    this.restrictions.forEach(restriction => {
      if (restriction.isEnable()) {
        restriction.clear();
      }
    });
  }
}
