import { RestrictionOperator } from './restriction-operator.enum';
import { RestrictionPattern } from './restriction-pattern.enum';

export interface IRestriction<T> {
  attribute: string;
  operator: RestrictionOperator;
  value?: T;
  values?: T[];
  valueStart?: T;
  valueEnd?: T;
  pattern?: RestrictionPattern;
}

export class Restriction<T> {

  public attribute: string;
  public operator: RestrictionOperator;
  public value: T;
  public values: T[];
  public valueStart: T;
  public valueEnd: T;
  public pattern: RestrictionPattern;

  constructor(params: IRestriction<any>) {
      this.attribute = params.attribute;
      this.operator = params.operator;
      this.value = params.value;
      this.values = params.values;
      this.valueStart = params.valueStart;
      this.valueEnd = params.valueEnd;
      this.pattern = params.pattern;
  }

  public clear(): void {
    this.value = undefined;
    this.values = [];
    this.valueStart = undefined;
    this.valueEnd = undefined;
  }

  public isNullOrEmpty(value: any): boolean {

    let result;

    if (typeof value === 'string') {
      result = value === undefined || String(value).trim() === '';
    }
    else {
      result = value === undefined;
    }

    return result;
  }

  public isEnable(): boolean {

    let result = false;

    if (this.isOperatorTypeIsNullOrIsNotNull()) {
      result = true;
    }
    else if (this.isOperatorTypeBetweenOrNotBetween()) {
      result = !this.isNullOrEmpty(this.valueStart) && !this.isNullOrEmpty(this.valueEnd);
    }
    else if (this.isOperatorTypeInOrNotIn()) {
      result = this.values !== undefined && this.values.length > 0;
    }
    else {
      result = !this.isNullOrEmpty(this.value);
    }

    return result;
  }

  public isOperatorTypeInOrNotIn(): boolean {
    return RestrictionOperator.IN === this.operator || RestrictionOperator.NOT_IN === this.operator;
  }

  public isOperatorTypeIsNullOrIsNotNull(): boolean {
    return RestrictionOperator.IS_NULL === this.operator || RestrictionOperator.IS_NOT_NULL === this.operator;
  }

  public isOperatorTypeBetweenOrNotBetween(): boolean {
    return RestrictionOperator.BETWEEN === this.operator || RestrictionOperator.NOT_BETWEEN === this.operator;
  }

  public isOperatorTypeEqualsOrNotEquals(): boolean {
    return RestrictionOperator.EQUALS === this.operator || RestrictionOperator.NOT_EQUALS === this.operator;
  }

  public onChangeOperator(): void {
    if (this.isOperatorTypeInOrNotIn()) {
      this.valueStart = this.valueEnd = this.value = null;
    }
    else if (this.isOperatorTypeBetweenOrNotBetween()) {
      this.value = null;
      this.values = [];
    }
    else if (this.isOperatorTypeIsNullOrIsNotNull()) {
      this.valueStart = this.valueEnd = this.value = null;
      this.values = [];
    }
    else {
      this.valueStart = this.valueEnd = null;
      this.values = [];
    }
  }
}
