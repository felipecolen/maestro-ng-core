import { Observable } from 'rxjs';

export interface IConfirmationService {
    confirm(model: ConfirmationModel): void;
}

export interface ConfirmationModel {
  title?: string;
  message: string;
  accept?: () => void;
  reject?: () => void;
}
