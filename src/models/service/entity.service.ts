import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SimplePagedQuery } from '../query/simple-paged-query.model';
import { PagedQueryResult } from '../query/paged-query-result.model';
import { HttpHeadersUtils } from '../../utils/http-headers-utils.model';

export abstract class EntityService<E, I> {

  constructor(protected http: HttpClient) {}

  public abstract getBasePath(): string;
  public abstract newEntity(): E;
  public abstract getId(entity: E): I;

  public path(path: string): string {

    let basePath = this.getBasePath();

    if (basePath.endsWith('/')) {
      basePath = basePath.substring(0, -1);
    }

    if (!path.startsWith('/')) {
      path = '/' + path;
    }

    return basePath + path;
  }

  public createJsonHttpHeaders() {
    return HttpHeadersUtils.contentTypeJson();
  }

  public isTransient(entity: E): boolean {
    const id = this.getId(entity);
    return typeof id === 'undefined';
  }

  public retrieveById(id: I): Promise<E> {
    return this.http.get<E>(this.path(`/retrieveById/${id}`))
      .toPromise();
  }

  public retrievePaged(simplePagedQuery: SimplePagedQuery): Promise<PagedQueryResult<E>> {

    const url = this.path('/retrievePaged');
    const json = JSON.stringify(simplePagedQuery);

    return this.http.post<PagedQueryResult<E>>(url, json, { headers: this.createJsonHttpHeaders() })
      .toPromise();
  }

  public retrieveSuggestions(term: string): Promise<E[]> {
    return this.http.get<E[]>(this.path(`/retrieveSuggestions?term=${term}`))
      .toPromise();
  }

  public retrieveItems(): Promise<E[]> {
    return this.http.get<E[]>(this.path('/retrieveItems'))
      .toPromise();
  }

  public delete(entity: E): Promise<void> {
    if (!this.isTransient(entity)) {
      return this.http.delete<void>(this.path(`/delete/${this.getId(entity)}`))
        .toPromise();
    }
    else {
      return of(undefined).toPromise();
    }
  }

  public save(entity: E): Promise<E> {

    const headers = { headers: this.createJsonHttpHeaders() };

    if (this.isTransient(entity)) {
      return this.http.post<E>(this.path('/insert'), JSON.stringify(entity), headers)
        .toPromise();
    }
    else {
      return this.http.put<E>(this.path('update'), JSON.stringify(entity), headers)
        .toPromise();
    }
  }
}
