import { IConfirmationService } from '../service/confirmation-service.interface';
import { Frm } from './frm.interface';
import { EntityService } from '../service/entity.service';
import { ExceptionResponse } from '../exception-response.model';
import { IMessageService } from '../..';

export interface FrmAssociationOneToManyConfigs {
  oneToManyAttributeName: string;
  manyToOneAttributeName: string;
  deleteTitle: string;
  deleteMessage: string;
}

export abstract class FrmAssociationOneToMany<E, EF extends Frm<E>, A, AS extends EntityService<A, any>> {

  private configs: FrmAssociationOneToManyConfigs;
  private associationSelected: A;

  constructor(
    protected frmEntity: EF,
    protected associationService: AS,
    protected messageService: IMessageService,
    protected confirmationService: IConfirmationService
  ) {
      this.configs = this.initConfigs();
  }

  public startInsert(): void {
    this.startInsertAction();
  }

  public startInsertAction(): A {

    const association = this.associationService.newEntity();

    const entity = this.frmEntity.getEntity();

    let associations = this.getAssociations(entity);

    associations = [...associations, association];

    this.updateEntityAssociations(associations);

    return association;
  }

  public updateEntityAssociations(associations: A[]): void {

    const entity = this.frmEntity.getEntity();

    (entity as any)[this.configs.oneToManyAttributeName] = [...associations];
  }

  public getAssociations(entity: any): A[] {

    let associations = entity[this.configs.oneToManyAttributeName];

    if (associations === undefined) {
      associations = [];
    }

    return associations;
  }

  public connect(association: A, entity: E): void {
    (association as any)[this.configs.manyToOneAttributeName] = entity;
  }

  public abstract initConfigs(): FrmAssociationOneToManyConfigs;

  public startDelete(associationSelected: A): void {

    this.associationSelected = associationSelected;

    this.confirmationService.confirm({
      title: this.configs.deleteTitle,
      message: this.configs.deleteMessage,
      accept: () => this.delete(),
      reject: () => this.cancelDelete()
    });
  }

  public beforeDelete(association: A, entity: E): void {
    // Change
  }

  public afterDelete(association: A, entity: E): void {
    // Change
  }

  public delete(): void {

    const association = this.associationSelected;
    const entity = this.frmEntity.getEntity();

    this.beforeDelete(association, entity);

    if (this.associationService.getId(association) !== null) {
      this.associationService.delete(association)
        .then(() => {
          this.deleteAction(association, entity);
        })
        .catch(response => {
          const exceptionResponse = response.json() as ExceptionResponse;
          this.messageService.addError('Erro', exceptionResponse.message);
        });
    }
    else {
      this.deleteAction(association, entity);
    }
  }

  public cancelDelete(): void {
    this.associationSelected = undefined;
  }

  private deleteAction(association: A, entity: E): void {

    const associations = this.getAssociations(entity);

    const index = associations.indexOf(association);

    if (index > -1) {
      associations.splice(index, 1);
      this.updateEntityAssociations(associations);
    }

    this.afterDelete(association, entity);

    this.cancelDelete();
  }
}
