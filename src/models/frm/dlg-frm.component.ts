import { Frm } from './frm.interface';

export class DlgFrm<F extends Frm<E>, E> {

  visible = false;

  constructor(
    private frm: F
  ) {}

  startInsert(entity?: E): void {
    this.frm.startInsert(entity);
    this.visible = true;
  }

  startUpdate(entity: E): void {
    this.frm.startUpdate(entity);
    this.visible = true;
  }

  save(): Promise<E> {
   return this.frm.save()
      .then(entity => {
        // TODO - Mostrar mensagem de sucesso
        console.log('Mostrar mensagem de sucesso!');

        this.visible = false;

        return entity;
      });
  }

  cancel(): void {
    this.frm.cancel();
    this.visible = false;
  }
}
