import { IProgressService } from './../service/progress-service.interface';
import { EntityService } from './../service/entity.service';
import { Frm } from './frm.interface';
import { IMessageService } from '../service/message-service.interface';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { ExceptionResponse } from '../exception-response.model';

export class BaseFrmComponent<S extends EntityService<E, I>, E, I> implements Frm<E> {

  public entity: E;

  protected state: FrmState;
  protected msgs = {
    error: {
      retrieveById: 'Erro ao recuperar o registro pelo identificador',
      save: 'Erro ao salvar o registro'
    },
    success: {
      save: 'O registro foi salvo com sucesso'
    }
  };

  constructor(
    protected service: S,
    protected messageService: IMessageService,
    protected progressService: IProgressService
  ) {
    this.cancel();
  }

  public startUpdate(entity: E): void {
    if (!this.service.isTransient(entity)) {
      this.startUpdateById(this.service.getId(entity));
    }
    else {
      this.startUpdateAction(entity);
    }
  }

  public startUpdateById(id: I): void {
    this.progressService.showModal();
    this.service.retrieveById(id)
      .then(entity => {
        this.startUpdateAction(entity);
        this.progressService.hide();
      })
      .catch(response => {
        const exceptionResponse = response.error as ExceptionResponse;
        this.messageService.addError(this.msgs.error.retrieveById, exceptionResponse.message);
        this.progressService.hide();
      });
  }

  public startInsert(entity?: E): void {

    if (!entity) {
      entity = this.service.newEntity();
    }

    this.entity = entity;
    this.state = FrmState.INSERTING;
    this.starting();
    this.startingInsert();
  }

  public isInactive(): boolean {
    return FrmState.INACTIVE === this.state;
  }

  public isInserting(): boolean {
    return FrmState.INSERTING === this.state;
  }

  public isUpdating(): boolean {
    return FrmState.UPDATING === this.state;
  }

  public cancel(): void {
    this.entity = this.service.newEntity();
    this.state = FrmState.INACTIVE;
    this.canceling();
  }

  public save(): Promise<E> {
    this.progressService.showModal();
    return this.service.save(this.entity)
      .then(salved => {
        this.entity = salved;
        this.messageService.addSuccess('Sucesso', this.msgs.success.save);
        this.progressService.hide();
        return salved;
      })
      .catch(response => {
          const exceptionResponse = response.error as ExceptionResponse;
          this.messageService.addError(this.msgs.error.save, exceptionResponse.message);
          this.progressService.hide();
          return undefined;
      });
  }

  public getEntity(): E {
    return this.entity;
  }

  protected starting(): void {}
  protected startingInsert(): void {}
  protected startingUpdate(): void {}
  protected canceling(): void {}

  private startUpdateAction(entity: E): void {
    this.entity = entity;
    this.state = FrmState.UPDATING;
    this.starting();
    this.startingUpdate();
  }
}

export enum FrmState {
  INACTIVE,
  UPDATING,
  INSERTING
}
