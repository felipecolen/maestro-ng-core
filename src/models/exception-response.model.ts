export class ExceptionResponse {
  public type: string;
  public message: string;
}
